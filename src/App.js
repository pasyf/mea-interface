import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import {Card, CardContent, Container, Button} from '@material-ui/core';
import "./App.css";

import csv from 'csv';

class App extends Component {

    onDrop(files) {

        this.setState({ files });

        var file = files[0];

        const reader = new FileReader();
        reader.onload = () => {
            csv.parse(reader.result, (err, data) => {

                var userList = [];

                for (var i = 0; i < data.length; i++) {
                    const name = data[i][0];
                    const value = data[i][1];
                    const newUser = { "name": name, "value": value };
                    console.log(newUser);
                    userList.push(newUser);

                    fetch(process.env.REACT_APP_UPLOAD_FILE, {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(newUser)
                    })
                };
            });
        };

        reader.readAsBinaryString(file);
    }

    render() {

        const wellStyles = { maxWidth: 400, margin: '0 auto 10px' };
        const fontSize = 5;

        return (
            <div className={"myclass"} align="center" oncontextmenu="return false">
                <Container>
                    <Card elevation={4}>
                        <CardContent>
                            <br /><br /><br />
                            <div className="dropzone">
                                <Dropzone accept=".csv" onDropAccepted={this.onDrop.bind(this)}>
                                </Dropzone>
                                <br /><br /><br />
                            </div>
                            <h2>Fournissez vos données au format <font size={fontSize} color="#3f51b5">CSV</font><br /></h2>
                            <Button variant={'contained'} color={"primary"}>Valider</Button>
                        </CardContent>
                    </Card>
                </Container>
            </div>
        )
    }
}

export default App;
