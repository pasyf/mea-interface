import React from "react";
import { useDropzone } from "react-dropzone";
import { makeStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

const useStyles = makeStyles({
    root: {
        marginBottom: 40,
        marginRight: 30,
        marginLeft: 30,
        marginTop: 20,

        height: "10em"
    }
});

const Dropzone = ({ onDrop, accept }) => {
    const classes = useStyles();

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        accept
    });

    const getClassName = (className, isActive) => {
        if (!isActive) return className;
        return `${className} ${className}-active`;
    };

    return (
        <div className={getClassName("dropzone", isDragActive)} {...getRootProps()}>
            <input className="dropzone-input" {...getInputProps()} />
            <Card className={classes.root} elevation={isDragActive ? 1 : 3}>
                {isDragActive ? (
                    <CardContent>
                        <p className="dropzone-content">Relâchez le fichier ici</p>
                    </CardContent>
                ) : (
                    <CardContent>
                        <p className="dropzone-content">Déposez glissez le fichier ici ou cliquez simplement</p>
                    </CardContent>
                )}
            </Card>
        </div>
    );
};

export default Dropzone;
